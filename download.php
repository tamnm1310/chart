<?php
echo "new code 04";

require_once 'classes/conf.php';
require_once 'classes/dao.php';
require_once 'classes/Membership.php';

$membership = new Membership();
$membership->confirm_Member();


include_once('classes/util.php');
// check & create export directory
checkDirectory(EXPORT_DIR);
removeOldFile(EXPORT_DIR);

$path = $_GET['p'];
$s = $_GET['s'];
$s = date('Y-m-d H:i:s', $s); 
$e = $_GET['e'];
$e = date('Y-m-d H:i:s', $e); 
$deviceId = urldecode($_GET['i']);

if (isValidFilename($path) || $e < $s || $e || $s ) {
	$dao = new Dao();
	$data = $dao->getDeviceFullInfo($deviceId, $s, $e, ' Limit 1');
	// extract first row to get Col-list
	$sample = reset($data);
	$_data = explode(';', $sample['Data']);

	$collist = array();
	if (is_array($_data)) {
		foreach ($_data as $arr) {
			$arr = explode('=', $arr);
			$collist [] = $arr[0];
		}
	}
	
	

	// create/open file for write
	$file = EXPORT_DIR.'/'.$path;
	$f = fopen($file, 'w');

	$header = array(
		'DeviceID',
		'RecordDate',
		'LoggerRecordNo'
	);
	foreach ($collist as $col) {
		$header []= $col;
	}
	$str = join(',', $header);
	fwrite($f, $str."\n");
	$mysql_res = $dao->getAllDeviceData($deviceId, $s, $e);
	while ($row = mysql_fetch_assoc($mysql_res)) {
		
		$device_data = $row['Data'];
		$device_data = explode(';', $device_data);
		$data = array(
			$row['DeviceID'],
			$row['RecordDate'],
			$row['LoggerRecordNo']
		);
		foreach ($device_data as $val) {
			$val = explode('=', $val);
			$data []= $val[1];
		}
		$str = join(',', $data);
		fwrite($f, $str."\n");
	}
	fclose($f);
	
	
    if (is_file($file)) {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-Type: application/x-msdownload; charset=utf-8');
        header("Content-Disposition: attachment; filename=$path");
        // Read the file from disk
        readfile($file);
    } else {
        echo 'Sorry, your requested file could not be found.';
    }
} else {
    echo 'Please do not edit URL by yourself.';
}

?>