<?php

require_once 'conf.php';
require_once 'dao.php';

class Membership {
	private $sys_superuser = array('admin');
	private $superPermission = 'super';
	
	private $systemPermssions = array(
		'userMgr' => 'Manage user',
		'deviceMgr' => 'Manage device',
		'super' => 'Super User'
	);
	
	public function Membership() {
		$this->dao = new Dao();
	}
	public function validate_user($un, $pwd) {
		@session_start();
		$user = $this->dao->getUserByUnAndPwd($un, $this->hash($pwd));
		if($user !== false)
		{
			$_SESSION['status'] = 'authorized';
			$_SESSION['user'] = $user;
			header("location: demo/".LOGIN_REDIRECT); 
			exit(1);
		} else return "Please enter a correct username and password";
		
	} 
	
	public function log_User_Out() {
		session_start();
		if(isset($_SESSION['status'])) {

			unset($_SESSION['status']);
			unset($_SESSION['user']);

			if(isset($_COOKIE[session_name()])) 
				setcookie(session_name(), '', time() - 1000);
				session_destroy();
		}
	}
	
	public function confirm_Member() {
		@session_start();
		if(empty($_SESSION['status']) || $_SESSION['status'] !='authorized') {
			header("location: ../login.php"); 
			die();
		}
		//Refresh permission		
		$user = $this->dao->getUserByUserName($_SESSION['user']->UserName);
		if($user !== false) {
			$_SESSION['user'] = $user;
		}else { //User has been deleted before
			header("location: ../login.php"); 
			die();
		}
	}
	
	public function hash($pwd) {
		$pwd = substr(md5($pwd.PASSWORD_SALT), 0, 25).substr(md5(PASSWORD_SALT),0,5);
		return $pwd;
	}
	
	public function can_User_Access_System_Settings($perm='') {
		if(session_id() == '') { 
			session_start(); 
		}
		// alway allow admin
		if (empty($_SESSION['user'])) return false;
		if (in_array($_SESSION['user']->UserName, $this->sys_superuser)) return true;

		$userPerm = $_SESSION['user']->SystemPermission;
		$userPerm = explode(',', $userPerm);
		if ($perm == '') {
			foreach ($userPerm as $p) {
				if (array_key_exists($p, $this->systemPermssions)) return true;
			}
		} else {
			if (in_array($perm, $userPerm)) return true;
		}
		return false;
	}
	
	public function getSystemPermission(){
		return $this->systemPermssions;
	}
	public function isSuperUser() {
		$userPerm = $_SESSION['user']->SystemPermission;
		$userPerm = explode(',', $userPerm);
		return in_array($this->superPermission, $userPerm);
	}
	public function getUserName() {
		if (!empty($_SESSION['user'])) {
			return $_SESSION['user']->UserName;
		}
		return '';
	}
	
	public function canModifyDeviceData($deviceID) {
		$permission = $this->dao->getPermissionOnDevice($deviceID, $_SESSION['user']->UserName);
		if (empty($permission)) return false;
		return $permission[0]['CanModify']==="1";
	}
	
	public function canReadDeviceData($deviceID) {
		$permission = $this->dao->getPermissionOnDevice($deviceID, $_SESSION['user']->UserName);
		if (empty($permission)) return false;
		return $permission[0]['CanModify']==="1";
	}
	
	public function canManageDevice($deviceID) {
		$permission = $this->dao->getPermissionOnDevice($deviceID, $_SESSION['user']->UserName);
		if (empty($permission)) return false;
		return $permission[0]['DeviceManager']==="1";
	}
}
?>