<?php

/*
 * Remove all files inside given directory
 * @param string $dir
 */
function cleanUp($dir) {
    $files = glob($dir.'/*.*'); // get all file names
    foreach($files as $file){
        if(is_file($file)) unlink($file);
    }
}

/*
* check directory have exists? if not exists then create new directorys
* @param string $dir
*/
function checkDirectory($dir)
{
    $path = explode('/', $dir);
    $total = count($path);
    $p = '';

    for ($i=0; $i<$total; $i++) {
        if (empty($path[$i])) {
            $p = '/';
            continue;
        }

        $p .= $path[$i].'/';

        if (!is_dir($p)) {
            mkdir($p, FOLDER_PERMISSION);
        } else {
            if (!is_writeable($p)) {
                @chmod($p, FOLDER_PERMISSION);
            }
        }
    }
}

/*
 * check if given filename has forbiden characters or not
 * @param string $filename
 *
 * @return boolean
 */
function isValidFilename($filename) {
    $notAllow = array(
        '..',
        '/',
        '\\',
        '#', '@', '$', '!', '~', '^', '&', '*', '/', '\\', '[', ']', '{', '}'
    );
    foreach ($notAllow as $str) {
        if (strpos($filename, $str) !== false) return false;
    }

    return true;
}


function convert($str){
	$str = str_replace('+', ' ', $str);
	$str = mysql_real_escape_string(urldecode($str));
	return $str;
}

function __getParam($index, $source='', $default='') {
	if ($source == '') $source = $_REQUEST;
	if (@array_key_exists($index, $source)) return $source[$index];
	else return $default;
}

function removeOldFile($dir) {
	$files = glob($dir.'/*.*'); // get all file names

	$lastweek = time() - 604800; // 60*60*24*7
	$lastweek = time() - 1; // 60*60*24*7
	foreach($files as $file){
        $time = explode('_', $file);
		$time = $time[1];
		
		$y = substr($time, 0, 4);
		$m = substr($time, 4, 2);
		$d = substr($time, 6, 2);
		$h = substr($time, 8, 2);
		$i = substr($time, 10, 2);
		$s = substr($time, 12, 2);
		
		$time = strtotime($y.'-'.$m.'-'.$d.' '.$h.':'.$i.':'.$s);
		if ($lastweek > $time) {
			if(is_file($file)) unlink($file);
		}
    }
}

?>