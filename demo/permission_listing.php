<?php
require_once '../classes/conf.php';
require_once '../classes/dao.php';
require_once '../classes/Membership.php';
$dao = new Dao();
$un ='';
$membership = new Membership();
$membership->confirm_Member();
if (!empty($_GET['u'])) {
	$un = htmlspecialchars($_GET['u'], ENT_QUOTES);
}
if (!empty($un)) {
	$users = $dao->getUser($un);
} else {
	$users = $dao->getAllUsers();
}
$userName = $_SESSION['user']->UserName;
$groups  = $dao->getAllManagableGroup($userName, $users);
$permission_group = $dao->getAllManageablePermissionGroup($userName, $users);
$isSuperUser = $membership->isSuperUser();
$permissions = $dao->getPermissions();

function getPermission($permission_group, $userName, $groupID) {
	foreach ($permission_group as $pg) {
		if ($pg->UserName === $userName && $pg->GroupID ===$groupID) {
			return $pg;
		}
	}
	return null;
}
?>
<div class="permission_list">
<form id="permission" name="permission">
<?php foreach ($users as $user): ?>
<h1> Permission Configuration for: <?= $user->FullName; ?> </h1>
<table class="table table-bordered user-permission-manager" id="<?=$user->UserName;?>">
	<tr>
		<td style="width:200px">
			<h2> System Permission </h2>
		</td>
		<td>
			<label class="checkbox inline"> <input type="checkbox" <?php if (strrpos($user->SystemPermission, "userMgr")!==false) echo 'checked'; ?> id="usermanager_<?=$user->UserName;?>" <?php if($user->UserName===$userName || !$isSuperUser) echo 'disabled';?>> Manage User </label>
			<label class="checkbox inline"> <input type="checkbox" <?php if (strrpos($user->SystemPermission, "deviceMgr")!==false) echo 'checked'; ?> id="devicemanager_<?=$user->UserName;?>" <?php if($user->UserName===$userName || !$isSuperUser) echo 'disabled';?>> Manage Device</label>
			<span class="label label-warning"><label class="checkbox inline"> <input type="checkbox" <?php if (strrpos($user->SystemPermission, "super")!==false) echo 'checked'; ?> id="super_<?=$user->UserName;?>" <?php if($user->UserName===$userName || !$isSuperUser) echo 'disabled';?>> Super User</label></span>
		</td>
	</tr>
	<tr>
		<td>
			<h2> Group Permission </h2>
		</td>
		<td>
			<table class="table table-bordered">
			 <?php foreach ($groups as $group): ?>
			 <?php $pg = getPermission($permission_group, $user->UserName, $group->GroupID); ?>
				<tr>
					<td style="width:200px">
						<h6> <?=$group->GroupName;?></h6>
					</td>
					<td class="bordered td-group-permission" id="tdgp_<?= $group->GroupID;?>_<?=$user->UserName;?>" groupId="<?= $group->GroupID;?>">
						<label class="checkbox inline" title="User can read device data"> 
							<input id="gCanRead_<?= $group->GroupID;?>_<?=$user->UserName;?>" type="checkbox" <?php if (!empty($pg) && $pg->CanRead==1) echo 'checked'; ?>> Can Read 
						</label>
						<label class="checkbox inline" title="User can edit device data">
							<input id="gCanMod_<?= $group->GroupID;?>_<?=$user->UserName;?>" type="checkbox" <?php if (!empty($pg) && $pg->CanModify==1) echo 'checked'; ?>> Can Modify 
						</label>
						<label class="checkbox inline" title="User can manage user of group">
							<input id="gUserManager_<?= $group->GroupID;?>_<?=$user->UserName;?>" type="checkbox" <?php if (!empty($pg) && $pg->UserManager==1) echo 'checked'; ?>> Manage User 
						</label>
						<label class="checkbox inline" title="User can manage device">
							<input id="gDeviceManager_<?= $group->GroupID;?>_<?=$user->UserName;?>"type="checkbox" <?php if (!empty($pg) && $pg->DeviceManager==1) echo 'checked'; ?>> Manage Device
						</label>
					</td>
				</tr>
			<?php endforeach; ?>
			</table>				
		</td>
	</tr>
<table>
<?php endforeach; ?>
<form>
<a class="btn btn-primary" id="save">Save</a>
</div>
<script>
	$(document).ready(function(){
		$('#save').click(function(){
			var serializedtable = serializedata();
			$.post(
				'permission_dao.php',
				{
					data:serializedtable,
					op: 'groupPerm'
				},
				function (data) {
					alert(data);
				}
			);
			
			return false;
		});
	});
	function serializedata() {
		var data = [];
		$('table.user-permission-manager').each(function() {
			var permissions=[];
			var userPermission =[];
			var userName = $(this).attr('id');
			var sysUserManager = $('#usermanager_'+userName).is(':checked')?'1':'0';
			var sysDeviManager = $('#devicemanager_'+userName).is(':checked')?'1':'0';
			var sysSuperUser   = $('#super_'+userName).is(':checked')?'1':'0';
			userPermission.push(sysUserManager);
			userPermission.push(sysDeviManager);
			userPermission.push(sysSuperUser);
			userPermission.push(userName);
			var gPermissions =[];
			$(this).find('td.td-group-permission').each(function() {
				var gPermission =[];
				var postPhase = $(this).attr('id').substring(5); //tdgp_
				var gId = $(this).attr('groupId');
				var gCanRead_ 		= $('#gCanRead_'+postPhase).is(':checked')?'1':'0';
				var gCanMod_ 		= $('#gCanMod_'+postPhase).is(':checked')?'1':'0';
				var gUserManager_ 	= $('#gUserManager_'+postPhase).is(':checked')?'1':'0';
				var gDeviceManager_ = $('#gDeviceManager_'+postPhase).is(':checked')?'1':'0';
				gPermission.push(gId);
				gPermission.push(gCanRead_);
				gPermission.push(gCanMod_);
				gPermission.push(gUserManager_);
				gPermission.push(gDeviceManager_);
				gPermissions.push(gPermission);
			});
			permissions.push(userPermission);
			permissions.push(gPermissions);
			data.push(permissions);
		});
		return data;
	}
</script>