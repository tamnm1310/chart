<?php
require_once '../classes/Membership.php';
require_once '../classes/conf.php';
require_once '../classes/dao.php';

$dao = new Dao();
$membership = new Membership();
$groups  = $dao->getAllDeviceGroups();
$devices = $dao->getAllDevices();

foreach ($devices as $d) {
	$groups[$d->DeviceGroupID]->child []= $d;
}

?>
<div class="data_listing">
    <h2>All devices/groups</h2>
    <table width="100%">
    	<tr class="header">
        	<td>ID</td>
            <td>Name</td>
            <td>Action</td>
        </tr>
        
        <?php foreach ($groups as $g): ?>
        <tr id="tr_group_<?= $g->GroupID; ?>" class="sub_header">
            <td><?= $g->GroupID; ?></td>
            <td><?= $g->GroupName; ?></td>
            <td style="width:173px;">
            	<a href="#" class="edit_group" data-id="<?= $g->GroupID; ?>">Edit</a>
                &nbsp;
                <a href="#" class="delete_group" data-id="<?= $g->GroupID; ?>">Remove</a>
                &nbsp;
				<a href="#" class="add_device" group-id="<?= $g->GroupID; ?>">Add Device</a>
				
                <input type="hidden" id="<?= $g->GroupID; ?>_GroupID" value="<?= $g->GroupID; ?>" />
                <input type="hidden" id="<?= $g->GroupID; ?>_GroupName" value="<?= $g->GroupName; ?>" />
                <input type="hidden" id="<?= $g->GroupID; ?>_GroupDescription" value="<?= $g->GroupDescription; ?>" />
                <input type="hidden" id="<?= $g->GroupID; ?>_GroupLogo" value="grp_image.php?f=l&g_id=<?= $g->GroupID; ?>" />
                <input type="hidden" id="<?= $g->GroupID; ?>_GroupMaps" value="grp_image.php?f=m&g_id=<?= $g->GroupID; ?>" />
                
                <input type="hidden" class="reserve_gi" id="<?= $g->GroupID; ?>" value="<?= $g->GroupID; ?>" />
            </td>
        </tr>
        <?php if (isset($g->child)): ?>
			<?php foreach ($g->child as $d): ?>
			<tr id="tr_dv_<?= $d->DeviceID; ?>">
				<td> &nbsp; - <?= $d->DeviceID; ?></td>
				<td><?= $d->DeviceName; ?></td>
				<td>
					<a href="#" class="edit_device" data-id="<?= $d->DeviceID; ?>">Edit</a>
					&nbsp;
					<a href="#" class="delete_device" data-id="<?= $d->DeviceID; ?>">Remove</a>
					
					<input type="hidden" id="<?= $d->DeviceID; ?>_DeviceID" value="<?= $d->DeviceID; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_DeviceGroupID" value="<?= $d->DeviceGroupID; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_DeviceName" value="<?= $d->DeviceName; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_DeviceDescription" value="<?= $d->DeviceDescription; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_DevicePosition" value="<?= $d->DevicePosition; ?>" />
					
					<input type="hidden" id="<?= $d->DeviceID; ?>_ChartRange" value="<?= $d->ChartRange; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_ChartRangeNotation" value="<?= $d->ChartRangeNotation; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_RecordInterval" value="<?= $d->RecordInterval; ?>" />
					<input type="hidden" id="<?= $d->DeviceID; ?>_MaxChartSample" value="<?= $d->MaxChartSample; ?>" />
					<input type="hidden" class="reserve_di" id="<?= $d->DeviceID; ?>" value="<?= $d->DeviceID; ?>" />
				</td>
			</tr>
			<?php endforeach; ?>
        <?php endif; ?>
        <?php endforeach; ?>
		<tr>
			<td colspan="3"><a href="#" class="add_group">Add Group</a></td>
		</tr>
    </table>
</div>

<div class="edit_form_wrapper" id="edit_group_region">
    <form id="group" enctype="multipart/form-data" action="device_dao.php" method="post" class="clearfix">
        <div class="line">
            Group ID <span class="required">*</span><br />
            <input type="text" id="groupid" name="groupid" />
            <input type="hidden" id="current_groupid" />
        </div>
        <div class="line">
            Group Name <span class="required">*</span><br />
            <input type="text" id="groupname" name="groupname" />
        </div>
        <div class="line">
            Group Description <br />
            <textarea id="groupdescription" name="groupdescription"></textarea>
        </div>
        
        <div class="fll" id="upload_group_image">
            <div class="line" id='div_grp_logo'>
                Group Logo <br />
				<input type="file" id="grouplogo" name="grouplogo">
            </div>
            <div class="line" id='div_grp_map'>
                Group Map<br />
                <input type="file" id="groupmaps" name="groupmaps">
            </div>
        </div>
        <div id="current_logo" class="fll" style=" width:150px; margin:0 5px">
        	Current Logo<small> <a href='#' class='remove_grp_logo' id='remove_grp_logo'>Remove</a></small><br />
            <img src="" id="cur_grouplogo" class="img-rounded" style="max-height:150px; max-width:150px;"/>
        </div>
        <div id="current_map" class="fll">
        	Current Map<small> <a href='#' class='remove_grp_map' id='remove_grp_map'>Remove</a></small><br />
            <img src="" id="cur_groupmaps" class="img-rounded" style="max-height:150px; max-width:150px;"/>
        </div>
        <div class="clearfloat"> </div>
        
        <a href="#" class="insert btn btn-primary">Add new group</a>
        <a href="#" class="update btn btn-primary">Update group</a>
        <a href="#" class="cancel btn">Cancel</a>
        
        <input type="hidden" name="op" id="op" />
        <input type="hidden" name="currentGI" id="currentGI" />
    </form>
</div>

<div class="edit_form_wrapper" id="edit_device_region">
	<form id="device">
        <div class="line share1">
            Device ID <span class="required">*</span><br />
            <input type="text" id="deviceid" name="deviceid" />
            <input type="hidden" id="current_deviceid" />
        </div>
        <div class="line share1">
            Device Group ID <span class="required">*</span><br />
			<select id="devicegroupid" name="devicegroupid">
            	<?php foreach ($groups as $g): ?>
                <option value="<?= $g->GroupID; ?>"><?= $g->GroupID; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="clearfloat"> </div>
        
        <div class="line">
            Device Name <span class="required">*</span><br />
            <input type="text" id="devicename" name="devicename" />
        </div>
        
        <div class="line">
            Device Description <br />
            <textarea id="devicedescription" name="devicedescription"></textarea>
        </div>
        <div class="line">
            Device Position <br />
            <input type="text" id="deviceposition" name="deviceposition" />
        </div>
        
        <div class="line share1">
            Default chart range<br />
				<input type="text" id="chartrange" name="chartrange" style="width:50%"/>
				<select id="chartrangenotation" name="chartrangenotation">
					<option value="Min">Minute(s)</option>
					<option value="Hour">Hour(s)</option>
					<option value="Day">Day(s)</option>
					<option value="Week">Week(s)</option>
					<option value="Mon">Month(s)</option>
					<option value="Year">Year(s)</option>
				</select>
			
        </div>
        <div class="line share1" title = "Record interval (minute) - time between two sample">
            Record interval<span class="required"> *</span><br />
            <input type="text" id="recordinterval" name="recordinterval" style="width:30px;" /> minute(s)
        </div>
		<div class="line share1" title = "Maximum available sample per Chart for this device">
            Max chart sample<br />
            <input type="text" id="maxchartsample" name="maxchartsample" style="width:60px;" /> Samples/Chart
        </div>
        <div class="clearfloat"> </div>
		
        <a href="#" class="insert btn btn-primary">Add new device</a>
        <a href="#" class="update btn btn-primary">Update device</a>
        <a href="#" class="cancel btn">Cancel</a>
    </form>
</div>
<div id="imgmapeditor" style="display: none;"></div>