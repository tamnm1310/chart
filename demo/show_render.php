<?php
require_once 'range_processing.php';
$_startDate = strtotime($startDate);
$_endDate = strtotime($endDate);
$ret = array();
if (!empty($deviceId)&&$_startDate&&$_endDate) {
	$detail = $dao->getDeviceChartDetail($deviceId);
    if (count($detail) == 0) {
		//No preconfigured chart, return no chart
    } else {
        $series = array();
		
        foreach ($detail as $info) {
            $_tmp = explode(',', $info['DataCol']);
			foreach ($_tmp as $_t) {
				$_t = trim($_t);
				$colName[$_t] = $_t;
			}
            $series[$info['DataCol']]['info'] = $info;
            $series[$info['DataCol']]['info']['StartDate'] = '';
        }
		// build WHERE string
		// just get MAX_CHART_SAMPLE record from StartDate to EndDate
		if ($includeTime==='range_dateonly') {
			$ret['_rd_startdate'] = date('Y-m-d', $_startDate);
			$ret['_rd_enddate'] = date('Y-m-d', $_endDate);
		}else {
			$ret['_rd_startdate'] = date('Y-m-d H:i:s', $_startDate);
			$ret['_rd_enddate'] = date('Y-m-d H:i:s', $_endDate);
		}
		
		$ret['_rd_rangecount'] = $_SESSION['chart_range'][$deviceId]['_rd_rangecount'];
		$ret['_rd_range_includetime'] = $_SESSION['chart_range'][$deviceId]['_rd_include_time'];
		$ret['_rd_rangetypenotation'] =$_SESSION['chart_range'][$deviceId]['_rd_rangetypenotation'];
		$ret['_rd_rangetypeselector'] = $_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'];
		
		$firstRow = $dao->getDeviceData($deviceId, " And RecordDate >= '".date('Y-m-d H:i:s', $_startDate)."' Order by RecordDate ASC Limit 1");
		$startDate = strtotime($startDate);
		$endDate = strtotime($endDate);
		foreach ($firstRow as $k => $v) {
			$startTime = $k;
			$startDate = strtotime($startTime);
			break;
		}
		$firstRow = $dao->getDeviceData($deviceId, " And RecordDate <= '".date('Y-m-d H:i:s', $_endDate)."' Order by RecordDate DESC Limit 1");
		foreach ($firstRow as $k => $v) {
			$endTime = $k;
			$endDate = strtotime($endTime);
			break;
		}
		// make date range
		$diff = round(abs($endDate - $startDate) / 60);
		$minuteStep = floor($diff/$maxChartSample);
		$where =' AND (`RecordDate` BETWEEN "' . date('Y-m-d H:i:s', $startDate) . '" AND "' . date('Y-m-d H:i:s', $endDate) . '") ';
		if ($minuteStep>0) {
			$where = ' AND (TIMESTAMPDIFF(MINUTE , "' . date('Y-m-d H:i:s', $startDate) . '", `RecordDate`)% ' . $minuteStep .' =0)' .$where;
		}
		//error_log("From ".date('Y-m-d H:i:s ', $startDate)." to ".date('Y-m-d H:i:s ', $endDate));
        $data = $dao->getDeviceData($deviceId, $where, ' order by RecordDate asc');
		if (count($data) > 0) {
			$colData = array();
			foreach ($data as $k => $val) {
				$row = explode(';', $val);

				foreach ($row as $col) {
					$col = explode('=', $col);

					if (in_array($col[0], $colName)) {
						$colData[$col[0]][] = (float)$col[1];
					}
				}
			}
			$dateKeys = array_keys($data);
			$firstTime = strtotime($dateKeys[0]);
			
			foreach ($detail as $k => $info) {
				$_col = explode(',', $info['DataCol']);
				$_min = array();
				foreach ($_col as $__c) {
					$__c = trim($__c);
					$series[$info['DataCol']]['chartdata'] []= array(
						'name' => $__c,
						'data' => $colData[$__c]
					);
					$_min []= !empty($colData[$__c]) ? min($colData[$__c]) : 0;
				}
				
				$series[$info['DataCol']]['info']['StartDate'] = $firstTime;
				$countMin = count($_min);
				
				if ($countMin > 1) {			
					$series[$info['DataCol']]['info']['yAxis_min'] = min($_min);
				}else if ($countMin==1){
					$series[$info['DataCol']]['info']['yAxis_min'] = $_min[0];
				}else {
					$series[$info['DataCol']]['info']['yAxis_min'] = 0;
				}
				$series[$info['DataCol']]['info']['yAxis_min'] = -2;
				$series[$info['DataCol']]['info']['yAxis_max'] = 38;
			}
			// sort data by Order
			foreach ($series as $col => $arrInfo) {
				$_series[] = $series[$col];
			}
			$ret['total'] = count($detail);
			$ret['data'] = $_series;
			$ret['minuteStep'] = $minuteStep*60000;
			$ret['title'] = 'Plot graph for #'.$deviceId;
			
		} else {
			//There is no data with the input range
			$ret['total'] = 0;			
			$ret['minuteStep'] = 60000;
			$ret['title'] = 'Plot graph for #'.$deviceId;
		}
    }//charts are configured well
}
// Set the JSON header
header("Content-type: text/json");
echo json_encode($ret);
?>