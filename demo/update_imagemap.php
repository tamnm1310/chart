<?php
	require_once '../classes/conf.php';
	require_once '../classes/util.php';
	require_once '../classes/dao.php';
	$dao = new Dao();
	$groupID = __getParam('_id');
	$image_link = __getParam('imgLink');
	$image_map = str_replace('\\', '', $_POST['imagemap']);
	
	$data = array();
	$data['GroupID'] = $groupID;
	$data['InteractiveMaps'] = $image_map;
	$dao->updateGroupImgMap($data);
?>
