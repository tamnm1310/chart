<?php
// Set the JSON header
require_once 'range_processing.php';

if (!empty($deviceId)) :
    $_SESSION['DeviceID'] = $deviceId;
    
    $max_record = 100; // show only first 100 record ??? @Tam: WHAT THE FUCK ???
    
    include_once('../classes/conf.php');
	include_once('../classes/dao.php');
    include_once('../classes/util.php');
	$removing = __getParam('r');
	$page_no = __getParam('p');
	$start=0;
	$total_page=0;
	
	if (!empty($page_no)) {
		$start = ((int)$page_no - 1) * 100;
		if ($start<0) $start=0;
	} else {
		$page_no = 1;
	}
	
	$dao = new Dao();
	
	$totalRec = $dao->countDeviceData($deviceId, $startDate, $endDate);	
	
	if ($totalRec > 0) {
		if ($totalRec < $max_record) {
			$limit = ' Limit 0, '.($totalRec-1);
		} else {
			$total_page = (int)($totalRec/$max_record) + ($totalRec%$max_record>0?1:0);
			if ($page_no >$total_page) $page_no = $total_page; //Refine the current page in case the data has been changed
			$start = ((int)$page_no - 1) * 100;
			$limit = ' Limit '. $start.', '.($max_record);
			
		}
		$data = $dao->getDeviceFullInfo($deviceId, $startDate, $endDate, $limit);
		// extract first row to get Col-list
		$sample = reset($data);
		$_data = explode(';', $sample['Data']);
	
		$collist = array();
		if (is_array($_data)) {
			foreach ($_data as $arr) {
				$arr = explode('=', $arr);
				$collist [] = $arr[0];
			}
		}
		
		ob_start();?>
		<div id="paginator_top" current_page ='<?= $page_no ?>' page_count='<?= $total_page ?>'>
		</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table table-bordered table-hover table-striped" id='datalogger_table' is_removing='<?php if ($removing) echo 'true';?>' deviceId='<?=$deviceId;?>'>
            <tr>
				<?php if ($removing) {  ?>
					<th style="width:130px"><a href="#" class="select_all" >Check all</a>/<a href="#" class="un_select_all">Uncheck all</a></th>
				<?php } else { ?>
					<th>#</th>
				<?php } ?>
                <th style="width:180px">RecordDate</th>
                <th style="width:100px">RecordNo</th>
                <?php foreach ($collist as $col): ?>
                <th><?= $col; ?></th>
                <?php endforeach; ?>
            </tr>
            <?php $i = 1; ?>
            <?php foreach ($data as $row): ?>
            <tr>
				<td align="center">
				<?php if ($removing) {  ?>
					<label class="checkbox inline" title="Select Item to delete">
					   <input type="checkbox" class="select_for_removal" recordNo='<?= $row['LoggerRecordNo']; ?>'><?= $start+$i++; ?>
					</label>
				<?php } else {?>
					<?= $start+$i++; ?>
				<?php } ?>
				</td>
                <td align="center"><?= $row['RecordDate']; ?></td>
                <td align="center"><?= $row['LoggerRecordNo']; ?></td>
                <?php 
                $_data = explode(';', $row['Data']); 
                $_record = array();
                foreach ($_data as $val) {
                    $_val = explode('=', $val); 
                    $_record[$_val[0]] = $_val[1];
                }
                ?>
                <?php foreach ($collist as $col): ?>
                <td align="center"><?= $_record[$col]; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </table>
		<?php if ($removing) { ?>
			<a class="btn btn-danger" href="#" id="btnRemoveDeviceData"><i class="icon-trash icon-white"></i> Delete</a>
		<?php } ?>
        <div id="paginator_bottom" class="pagination pagination-right pagination-mini" current_page ='<?= $page_no ?>' page_count='<?= $total_page ?>'>
		</div>
        <?php
            $html = ob_get_contents();
            ob_end_clean();
			if (!$removing) { 
				$fs = strtotime($startDate);
				$fe = strtotime($endDate);


				$notAllowed = array('#', '@', '$', '!', '~', '^', '&', '*', '/', '\\', '[', ']', '{', '}');
				$replace = '';
				$fileName = str_replace($notAllowed, $replace, $deviceId).'-'.date('YmdHis').'.csv&s='.$fs.'&e='.$fe.'&i='.urlencode($deviceId);
			}
			if ($removing) { 
				$ret = array(
					'title' => 'Tabular data for #'.$deviceId,
					'_rd_rangetypeselector' => $rangeTypeSelector
				);
			}else {
				$ret = array(
					'title' => 'Tabular data for #'.$deviceId,
					'csv_link' => $fileName, 
					'_rd_rangetypeselector' => $rangeTypeSelector
				);
			}
			if ($rangeTypeSelector == SELECT_BY_PERIOD ) {
				$ret['_rd_rangetypeselector'] = $_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'];
				$ret['_rd_rangetypenotation'] = $_SESSION['chart_range'][$deviceId]['_rd_rangetypenotation'];
				$ret['_rd_rangecount'] = $_SESSION['chart_range'][$deviceId]['_rd_rangecount'];
			} else {
				$ret['_rd_range_includetime'] = $_SESSION['chart_range'][$deviceId]['_rd_include_time'];
				if ($includeTime==='range_dateonly') {
					$ret['_rd_startdate'] = date('Y-m-d', strtotime($startDate));
					$ret['_rd_enddate'] = date('Y-m-d', strtotime($endDate));
				}else {
					$ret['_rd_startdate'] = date('Y-m-d H:i:s', strtotime($startDate));
					$ret['_rd_enddate'] = date('Y-m-d H:i:s', strtotime($endDate));
				}
			}
			$ret['html'] = $html;
		} else {
			$ret = array();
		}
else :
    $ret = array();
endif;

header("Content-type: text/json");
echo json_encode($ret);