<?php 
include_once('_header.php');

?>

<!-- Open DIV in _header.php file-->
    <div id="left">
        <h1>Devices</h1>
        
        <ul id="nav">
            <?php foreach ($groups as $g): ?>
            <li><a href="#" class="<?= $g->className; ?>" rel="g_<?= $g->GroupID; ?>_map"><?= $g->GroupName; ?></a>
                <?php if (!empty($g->child)): ?>
                <ul <?php if ($g->className == 'active'): ?>style="display:block"<?php endif; ?>>
                    <?php foreach ($g->child as $d): ?>
                    <li><a href="#" data-id="<?= $d->DeviceID; ?>" id="<?= $d->DeviceID; ?>" class="device"><?= $d->DeviceID; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div><!-- Open DIV in _header.php file-->

<div id="copyright">
    <p>Copyright © 2012-2013. All rights Reserved, VNTech Specialist Group </p>
    <p>Contact us at:  vietnamese.specialists@gmail.com</p>	
</div>