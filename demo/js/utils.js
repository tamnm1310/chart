var chart;
var pointInterval = 60000; // 1 minutes
var waitForReady = 300;

function __drawChart(data, options, addMiliseconds) {
	$('#chart_wrapper #container').removeClass('autoheight');
	
	$('#rd_startdate').val(data._rd_startdate);
	$('#rd_enddate').val(data._rd_enddate);
	
	$('#rangeTypeSelector').val(data._rd_rangetypeselector);
	$("#rangeTypeSelector").click();
	$('#rangeCount').val(data._rd_rangecount);
	$('#rangeNotation').val(data._rd_rangetypenotation);
	$('.addedContainer').remove(); //remove newly added container
	var includeDateTime = $('#range_include_time_option .' + data._rd_range_includetime);
	if (includeDateTime) {
		changeDateTimePickerTypeHandle(includeDateTime);
	}
	var chartData = data.data;
	// remove existed title
	$('h1.page_title').remove();
	$('#container').before('<h1 class="page_title">'+data['title']+'</h1>');
	try {
		var total1 = parseInt(data.total);
		if (total1<=0) { // no data found
			$('h1.page_title').html('No data found!');
			$('#container.chart_container').html('');
			return; 
		}
	}catch(Exception) {
		$('h1.page_title').html('No data found!');
		$('#container.chart_container').html('');
		return; 
	}
	if (data.minuteStep) pointInterval = data.minuteStep;
	var i=data.total;
	while (i > 1) { // create more container for new chart
		$('#container').after('<div id="container'+i+'" class="addedContainer chart_container"></div>');
		i--;
	}
	var w = $(window).width();
	if (w < 1000) {
		$('.chart_container').css({
			'width': '80%',
			'min-width': 'auto',
			'margin-left' : '6%'
		});
	}

	for (i=0; i<data.total; i++) {
		_options = chartData[i];

		options.chart.renderTo = 'container';
		if (i>0) options.chart.renderTo += (parseInt(i+1));

		
		if (_options.chartdata.length > 1) { // multiple sensors in one chart
			options.series = new Array;
			for (var j=0; j < _options.chartdata.length; j++) {
				var _object = new Object();
				_object.name = _options.chartdata[j].name;
				_object.data = _options.chartdata[j].data;
				_object.showInLegend = true;
				_object.pointInterval = pointInterval;
				_object.pointStart = parseInt(_options.info.StartDate*1000 - addMiliseconds);
				_object.marker = {enabled: false};
				
				options.series[j] = _object;
			}
		} else { // one sensor per chart
			options.series = [{
				'name': _options.chartdata[0].name,
				'data': _options.chartdata[0].data,
				'showInLegend': false,
				'pointInterval': pointInterval,
				//pointStart: Date.UTC(2012, 9, 22, 02, 03, 22)
				'pointStart': parseInt(_options.info.StartDate*1000 - addMiliseconds),
				'marker': {
					enabled: false
				}
			}];
		}
		options.title.text = _options['info']['Title'];
		options.subtitle.text = _options['info']['SubTitle'];
		options.subtitle.text = '';
		options.yAxis.title.text = _options['info']['YAxisLabel'];
		options.chart.type = _options['info']['Type'];
		options.chart.zoomType ='x';

		if (options.chart.type == 'line') {
			if (_options['info']['MinYValue'] && _options['info']['MaxYValue']) {
				options.yAxis.min = _options['info']['MinYValue'];
				options.yAxis.max = _options['info']['MaxYValue'];
			}else {
   			    options.yAxis.min = _options['info']['yAxis_min'];
   			    options.yAxis.max = _options['info']['yAxis_max'];
			}
		}

		chart = new Highcharts.Chart(options);
	}
}
$(document).ready(function() {
    var myDate = new Date();
    var addMiliseconds = myDate.getTimezoneOffset() * 60000;

    var options = {
        chart: {
            renderTo: 'container',
            type: 'line'
        },
        exporting: {
            url: '../export/index.php'
        },
        title: {
            text: 'Chart by TamNM',
            x: -20
        },
        subtitle: {
            text: 'for demo only',
            x: -20
        },
        yAxis: {
            title: {
                text: 'Temperature (C)'
            }
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: null
            }
        },
        plotOptions: {
            line: {
                lineWidth: 1
            }
        }
    };

    if (action == 'chart') {
		$('.device').click(function(e){
			var _id = $(this).attr('data-id');
			$.get(
				'show_render.php',
				{
					id: _id
				},
				function(data){
					__drawChart(data, options, addMiliseconds);
					
					$('#redrawChart').attr('data-id', _id);
					$('#redrawContainer').show();
				}, 'json'
			);
	
			var h = $(window).height();
			h = h - 150;
			$('#chart_wrapper').css({
				'height': h+'px',
				'overflow': 'auto'
			})
			
			showSelectDateblock();
			
			e.preventDefault();
		});
		
		$('#redrawChart').click(function(e){			
			var rangeTypeSelector = $('#rangeTypeSelector').val();
			var selected = $('#range_include_time_option').find('.active:first')[0];			
			if (rangeTypeSelector=='selectedByRange') {
				var start = $.trim($('#rd_startdate').val());				
				var end   = $.trim($('#rd_enddate').val());
				if (start == '' || end == '') {
					alert('Please select start date and end date');
					return false;
				}else {
					var selected = $('#range_include_time_option').find('.active:first')[0];
					var hasTime = ($(selected).attr('value'));
					$.get(
						'show_render.php',
						{
							id: $(this).attr('data-id'),
							rtl: rangeTypeSelector,
							ht: hasTime,
							s : start,
							e : end
						},
						function(data){
							__drawChart(data, options, addMiliseconds);
						}, 'json'
					);
				}
			}else if (rangeTypeSelector=='selectLastPeriod') {
				var rangeCount = $('#rangeCount').val();
				var rangeNotation  = $('#rangeNotation').val();
				if (isNaN(rangeCount)) {
					alert('Please give a number for the range!');
					return false;
				} else {
					$.get(
						'show_render.php',
						{
							id: $(this).attr('data-id'),
							rtl: rangeTypeSelector,
							rc : rangeCount,
							rn : rangeNotation
						},
						function(data){
							__drawChart(data, options, addMiliseconds);
						}, 'json'
					);
				}
			}
			
			var h = $(window).height();
			h = h - 150;
			$('#chart_wrapper').css({
				'height': h+'px',
				'overflow': 'auto'
			})	
			e.preventDefault();
		});
	}
    

    if (action == 'table') {
		$('.device').click(function(e){
			var _id = $(this).attr('data-id');
			
			__set_loading_state('#chart_wrapper #container');
			$.get(
				'table_render.php',
				{
					id: _id
				},
				function(data){
					__draw_tabular_data(data);
					
					$('#redrawChart').attr('data-id', _id);
				} , 'json'
			);
			
			showSelectDateblock();
			e.preventDefault();
		});

		
		$('#redrawChart').click(function(e){			
			var rangeTypeSelector = $('#rangeTypeSelector').val();
			var selected = $('#range_include_time_option').find('.active:first')[0];			
			if (rangeTypeSelector=='selectedByRange') {
				var start = $.trim($('#rd_startdate').val());				
				var end   = $.trim($('#rd_enddate').val());
				if (start == '' || end == '') {
					alert('Please select start date and end date');
					return false;
				}else {
					var selected = $('#range_include_time_option').find('.active:first')[0];
					var hasTime = ($(selected).attr('value'));
					
					__set_loading_state('#chart_wrapper #container');
					$.get(
						'table_render.php',
						{
							id: $(this).attr('data-id'),
							rtl: rangeTypeSelector,
							ht: hasTime,
							s : start,
							e : end
						},
						function(data){
							__draw_tabular_data(data);
						}, 'json'
					);
				}
			}else if (rangeTypeSelector=='selectLastPeriod') {
				var rangeCount = $('#rangeCount').val();
				var rangeNotation  = $('#rangeNotation').val();
				if (isNaN(rangeCount)) {
					alert('Please give a number for the range!');
					return false;
				} else {
					__set_loading_state('#chart_wrapper #container');
					$.get(
						'table_render.php',
						{
							id: $(this).attr('data-id'),
							rtl: rangeTypeSelector,
							rc : rangeCount,
							rn : rangeNotation
						},
						function(data){
							__draw_tabular_data(data);
						}, 'json'
					);
				}
			}

			e.preventDefault();
		});
	}
	if (action == 'remove_data') {
		$('.device').click(function(e){
			var _id = $(this).attr('data-id');
			
			__set_loading_state('#chart_wrapper #container');
			$.get(
				'table_render.php',
				{
					id: _id, 
					r: 'true'
				},
				function(data){
					__draw_tabular_data(data);
					
					$('#redrawChart').attr('data-id', _id);
				} , 'json'
			);
			
			showSelectDateblock();
			e.preventDefault();
		});

		
		$('#redrawChart').click(function(e){			
			var rangeTypeSelector = $('#rangeTypeSelector').val();
			var selected = $('#range_include_time_option').find('.active:first')[0];			
			if (rangeTypeSelector=='selectedByRange') {
				var start = $.trim($('#rd_startdate').val());				
				var end   = $.trim($('#rd_enddate').val());
				if (start == '' || end == '') {
					alert('Please select start date and end date');
					return false;
				}else {
					var selected = $('#range_include_time_option').find('.active:first')[0];
					var hasTime = ($(selected).attr('value'));
					
					__set_loading_state('#chart_wrapper #container');
					$.get(
						'table_render.php',
						{
							id: $(this).attr('data-id'),
							rtl: rangeTypeSelector,
							ht: hasTime,
							s : start,
							e : end,
							r: 'true'
						},
						function(data){
							__draw_tabular_data(data);
						}, 'json'
					);
				}
			}else if (rangeTypeSelector=='selectLastPeriod') {
				var rangeCount = $('#rangeCount').val();
				var rangeNotation  = $('#rangeNotation').val();
				if (isNaN(rangeCount)) {
					alert('Please give a number for the range!');
					return false;
				} else {
					__set_loading_state('#chart_wrapper #container');
					$.get(
						'table_render.php',
						{
							id: $(this).attr('data-id'),
							rtl: rangeTypeSelector,
							rc : rangeCount,
							rn : rangeNotation,
							r: 'true'
						},
						function(data){
							__draw_tabular_data(data);
						}, 'json'
					);
				}
			}

			e.preventDefault();
		});
	}
	setTimeout(function(){
        if (typeof DeviceID != 'undefined') {
			$('#'+DeviceID).trigger('click');
        }
    }, waitForReady);
	
	$('#nav > li > a').click(function(){
        if ($(this).attr('class') != 'active'){
            $('#nav li ul').slideUp();
            $(this).next().slideToggle();
            $('#nav li a').removeClass('active');
            $(this).addClass('active');
			
			removeCurrentDeviceIDSession();
			drawMaps($(this).attr('rel'));
			hideSelectDateblock();
        }
    });
	
});
function __set_loading_state(id) {
	$('h1.page_title').remove();
	$(id).html('<img src="images/loading.gif">');
}
function __draw_tabular_data(data) {
	// remove existed title
	$('h1.page_title').remove();

	var w = $(window).width();
	var h = $(window).height();
	w = w - 400;
	h = h - 300;
	$('#rd_startdate').val(data._rd_startdate);
	$('#rd_enddate').val(data._rd_enddate);
	
	$('#rangeTypeSelector').val(data._rd_rangetypeselector);
	$("#rangeTypeSelector").click();
	$('#rangeCount').val(data._rd_rangecount);
	$('#rangeNotation').val(data._rd_rangetypenotation);
	var includeDateTime = $('#range_include_time_option .' + data._rd_range_includetime);
	if (includeDateTime) {
		changeDateTimePickerTypeHandle(includeDateTime);
	}
	var beforeHTML = '<h1 class="page_title">'+data['title'] +'</h1>';
	if (data['csv_link']) {
		beforeHTML = '<h1 class="page_title">'+data['title']+' &nbsp; <a title="Download CSV file" href="../download.php?p='+data['csv_link']+'" id="downloadCSV"><img src="images/download.png" ></a></h1>';
	}
	$('#container').css({
		'width': w+'px',
		'height': h+'px',
		'max-height': h+'px',
		'margin': '0 0 0 60px',
		'overflow': 'auto'
	}).html(data['html']).before(beforeHTML);
	if (action == 'remove_data') {
		$('a.un_select_all').off('click');
		$('a.un_select_all').click(function() {
			$('table#datalogger_table .select_for_removal').removeAttr('checked');
		});
		
		$('a.select_all').off('click');
		$('a.select_all').click(function() {
			$('table#datalogger_table .select_for_removal').attr('checked', 'checked');
		});
		$('#btnRemoveDeviceData').off('click');
		$('#btnRemoveDeviceData').click(function() {
			var count = 0;
			if ($('table#datalogger_table .select_for_removal:checked').length<=0) {
				bootbox.alert("Nothing selected to delete!");
				return;
			}
			bootbox.confirm("Are you sure?", function(result) {
				var deletedRecordIds = [];
				var deviceID = $('#datalogger_table').attr('deviceId');
				if (result) {
					$('table#datalogger_table .select_for_removal:checked').each(function() {						
						if ($(this).attr("recordno").length>0) {
							deletedRecordIds.push($(this).attr("recordno"));
						}
					});
					$.post(
						'remove_data_dao.php',
						{
							Ids:deletedRecordIds,
							deviceID: deviceID
						},
						function (data) {
							alert(data);
							$.get(
								'table_render.php',
								{
									id: deviceID,
									r: true
								},
								function(data){
									__draw_tabular_data(data);
									$('#redrawChart').attr('data-id', deviceID);
								} , 'json'
							);
						}
					);//post deleted ids
				}
			}); 
		});
	}
	setupPaginator('#paginator_top');
	setupPaginator('#paginator_bottom');
}
function setupPaginator(divId) {
	//Render paginator
	var _currentPage = parseInt($(divId).attr('current_page'));
	var _totalPages  = parseInt($(divId).attr('page_count'));
	var options = {
		currentPage: _currentPage,
		numberOfPages: 5,
		totalPages: _totalPages,
		size:"mini",
        alignment:"right",
		itemContainerClass: function (type, page, current) {
			return (page === current) ? "active" : "pointer-cursor";
		},
		onPageClicked: function(e,originalEvent,type,page){
			var _id = $('table#datalogger_table').attr('deviceId');
			var is_removing = $('table#datalogger_table').attr('is_removing');
			$('h1.page_title').html('<img src="images/loading.gif">');
			if (is_removing) {
				$.get(
					'table_render.php',
					{
						id: _id,
						p: page, 
						r: true
					},
					function(data){
						__draw_tabular_data(data);
						$('#redrawChart').attr('data-id', _id);
					} , 'json'
				);
			}else {
				$.get(
					'table_render.php',
					{
						id: _id,
						p: page
					},
					function(data){
						__draw_tabular_data(data);
						$('#redrawChart').attr('data-id', _id);
					} , 'json'
				);
			}
		}
	}
	$(divId).bootstrapPaginator(options);
	$(divId).css('margin', '0px');
}
function removeCurrentDeviceIDSession() {
	$.post(
		'_header.php',
		{
			'removeCurrentDeviceIDSession': 1
		}
	);
	
	delete DeviceID;
}
function drawMaps(id) {
	if (typeof DeviceID == 'undefined') {
		if (typeof id == 'undefined') {
			var maps = $('.group_maps');
			var map = $(maps[0]);
		} else {
			var map = $('#'+id);
		}
		var imgmap = $(map).find('map')[0];
		var htmlSource;
		if (imgmap) {
			htmlSource = '<img src="'+$(map).attr('imgSource')+'" usemap="#'+imgmap.name+'" />';
			htmlSource = htmlSource + '<map name="' + imgmap.name + '">' + $(imgmap).html() + '</map>';
		}else {
			htmlSource = '<img src="'+$(map).attr('imgSource')+'" />';
		}
		
		$('#chart_wrapper #container').html(htmlSource).addClass('autoheight');
		$('.addedContainer').remove(); //remove newly added container
	}
}
function hideSelectDateblock() {
	$('#redrawContainer').hide();
	$('.page_title').hide();
}
function showSelectDateblock() {
	$('#redrawContainer').show();
	$('.page_title').show();
}
function changeDateTimePickerTypeHandle(obj) {
	var storedStartDate = $('#rd_startdate').val().trim();
	var storedEndDate = $('#rd_enddate').val().trim();
	
	$('#drawByRange').empty();
	$('#drawByRange').html(storedHTML);
	
	var s_id = 'datetimepickerstart_' + new Date().getTime();
	var e_id = 'datetimepickerend_' + new Date().getTime();
	$('#datetimepickerstart').attr('id', s_id);
	$('#datetimepickerend').attr('id', e_id);
	if ($(obj).attr("value") =='range_dateonly')  {
		$('#'+s_id).datetimepicker({
			format: 'yyyy-MM-dd',
			language: 'en'
		});
		$('#'+e_id).datetimepicker({
			format: 'yyyy-MM-dd',
			language: 'en'
		});
		$('#range_include_time_option .range_dateonly').addClass('active');
		$('#range_include_time_option .range_datetime').removeClass('active');
		$('#rd_startdate').val(storedStartDate);
		$('#rd_enddate').val(storedEndDate);
	}else {
		$('#'+s_id).datetimepicker({
			format: 'yyyy-MM-dd hh:mm:ss',
			language: 'en'
		});
		$('#'+e_id).datetimepicker({
			format: 'yyyy-MM-dd hh:mm:ss',
			language: 'en'
		});			
		$('#range_include_time_option .range_datetime').addClass('active');
		$('#range_include_time_option .range_dateonly').removeClass('active');
		$('#rd_startdate').val(storedStartDate);
		$('#rd_enddate').val(storedEndDate);
	}
	$('#range_include_time_option .btn-small').click(function() {
		changeDateTimePickerTypeHandle(this);
	});
}
$('#range_include_time_option .btn-small').click(function() {
	changeDateTimePickerTypeHandle(this);
});

function showDevice(_id) {
	var options = {
        chart: {
            renderTo: 'container',
            type: 'line'
        },
        exporting: {
            url: '../export/index.php'
        },
        title: {
            text: 'Chart by TamNM',
            x: -20
        },
        subtitle: {
            text: 'for demo only',
            x: -20
        },
        yAxis: {
            title: {
                text: 'Temperature (C)'
            }
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: null
            }
        },
        plotOptions: {
            line: {
                lineWidth: 1
            }
        }
    };
	var myDate = new Date();
    var addMiliseconds = myDate.getTimezoneOffset() * 60000;
	$.get(
		'show_render.php',
		{
			id: _id
		},
		function(data){
			__drawChart(data, options, addMiliseconds);
			
			$('#redrawChart').attr('data-id', _id);
			$('#redrawContainer').show();
		}, 'json'
	);
}