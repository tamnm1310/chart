function initEditDevice(){
	$('.delete_device').click(function(){
		if (confirm('Are you sure want to remove this device?')) {
			var _o = $(this);
			var id = _o.attr('data-id');
			$.get(
				'device_dao.php',
				{
					op: 'delete_device',
					deviceid: id
				},
				function(data){
					$('#tr_dv_'+id).remove();
				}
			);
		}
		return false;
	});
	
	$('.edit_device').click(function(){
		document.getElementById('device').reset();
		$('#edit_group_region').hide();
		$('#edit_device_region').show();
		var _o = $(this);
		var id = _o.attr('data-id');
		
		$('#deviceid').val($('#'+id+'_DeviceID').val()).attr('readonly', 'readonly');
		$('#devicegroupid').val($('#'+id+'_DeviceGroupID').val());
		$('#devicename').val($('#'+id+'_DeviceName').val());
		$('#devicedescription').html($('#'+id+'_DeviceDescription').val());
		$('#deviceposition').val($('#'+id+'_DevicePosition').val());
		
		$('#chartrange').val($('#'+id+'_ChartRange').val());
		$('#chartrangenotation').val($('#'+id+'_ChartRangeNotation').val());
		$('#recordinterval').val($('#'+id+'_RecordInterval').val());
		$('#maxchartsample').val($('#'+id+'_MaxChartSample').val());
		$('#current_deviceid').val(id);

		$('#device .insert').hide();
		$('#device .update').show();
		$('#device .cancel').show();
		
		
		return false;
	});
	$('#device .cancel').click(function(){
		document.getElementById('device').reset();
		$('#edit_device_region').hide();
		$('#edit_group_region').hide();
		$('#deviceid').removeAttr('readonly');
		return false;
	});
	$('.add_device').click(function() {
		document.getElementById('device').reset();
		$('#devicegroupid').val($(this).attr('group-id'));
		$('#deviceid').removeAttr('readonly');
		$('#device .insert').show();
		$('#device .update').hide();
		$('#device .cancel').show();
		$('#edit_device_region').show();
		$('#edit_group_region').hide();
	});
	$('#device .insert').click(function(){
		_checkDeviceForm(true);
	});
	$('#device .update').click(function(){
		_checkDeviceForm(false);
	});
}
function _checkDeviceForm(isInsert){
	var _deviceid = $('#deviceid');
	if (_deviceid.val() == '') {
		alert('Please enter device ID');
		return false;
	}
	// check if new user name is duplicate or not
	var deviceidArr = $('.reserve_di');
	if (!isInsert) { // no need to check current username
		var _deviceid_to_check = $('#current_deviceid').val();
		for (var i=0; i<deviceidArr.length; i++) {
			if ($(deviceidArr[i]).val() == _deviceid_to_check) {
				deviceidArr.splice(i, 1);
				break;
			}
		}
	}
	for (var i=0; i<deviceidArr.length; i++) {
		if (_deviceid.val() == $(deviceidArr[i]).val()) {
			alert('Given device ID is already taken. Please try again');
			return false;
		}
	}
	
	var _devicename = $('#devicename');
	if (_devicename.val() == '') {
		alert('Please enter device name');
		return false;
	}
	
	var _interval = $('#recordinterval');
	if (_interval.val() == '') {
		alert('Please enter record interval');
		return false;
	}
	if (isNaN(_interval.val())) {
		alert('Please enter record interval as an INTERGER (1, 2, 3,...)');
		return false;
	}
	
	var op = 'update_device';
	if (isInsert) op = 'insert_device';
	$.get(
		'device_dao.php',
		{
			data: $('#device').serialize(),
			op: op,
			currentDI: $('#current_deviceid').val()
		},
		function(data){
			if (data == 1) {
				if (isInsert) {
					alert('New device created');
				} else {
					alert('Device updated');
				}
				
				$('.sys_setting_wrapper').load('device_listing.php');
			} else {
				alert('Error occurs - pls try again');
			}
			document.getElementById('device').reset();
		}
	);
	
	return false;
}


function initEditGroup(){
	$('.delete_group').click(function(){
		if (confirm('Are you sure want to remove this group?')) {
			var _o = $(this);
			var id = _o.attr('data-id');
			$.get(
				'device_dao.php',
				{
					op: 'delete_group',
					groupid: id
				},
				function(data){
					$('#tr_group_'+id).remove();
				}
			);
		}
		
		return false;
	});
	
	$('.edit_group').click(function(){
		document.getElementById('group').reset();
		$('#edit_device_region').hide();
		$('#edit_group_region').show();
		var _o = $(this);
		var id = _o.attr('data-id');
		
		$('#groupid').val($('#'+id+'_GroupID').val()).attr('readonly', 'readonly');
		$('#groupname').val($('#'+id+'_GroupName').val());
		$('#groupdescription').html($('#'+id+'_GroupDescription').val());

		$('#cur_grouplogo').attr('src', $('#'+id+'_GroupLogo').val());
		$('#remove_grp_logo').attr('group_id', id);
		$('#cur_groupmaps').attr('src', $('#'+id+'_GroupMaps').val());
		$('#remove_grp_map').attr('group_id', id);
		$('#cur_groupmaps').unbind('click');
		$('#cur_groupmaps').attr('group_id', $('#'+id+'_GroupID').val());
		$('#div_grp_map').hide();
		$('#div_grp_logo').hide();
		
		$('#cur_groupmaps').error(function() {
			$('#current_map').hide();
			$('#div_grp_map').show();
		});
		$('#cur_grouplogo').error(function() {
			$('#current_logo').hide();
			$('#div_grp_logo').show();
		});
		
		$('#remove_grp_logo').click(function() {
			var _id = $(this).attr('group_id');
			$.get(
				'grp_image.php',
				{
					rm: 'rm',
					g_id: _id,
					f: 'l'
				},
				function(data){
					$('#current_logo').hide();
					$('#div_grp_logo').show();
				}
			);
		});
		$('#remove_grp_map').click(function() {
			var _id = $(this).attr('group_id');
			$.get(
				'grp_image.php',
				{
					rm: 'rm',
					g_id: _id,
					f: 'm'
				},
				function(data){
					$('#current_map').hide();
					$('#div_grp_map').show();
				}
			);
		});
		$('#cur_groupmaps').click(function(){
			var imagePath = $(this).attr('src');
			var groupID = $(this).attr('group_id');
			$.get(
				'group_devicemap.php',
				{
					imgLink: imagePath,
					g_id: groupID
				},
				function(data){
					$('#imgmapeditor').html(data);
					$('#imgmapeditor').siblings().hide()
					$('#imgmapeditor').parents().siblings().hide()
					$('#imgmapeditor').show();
					$('#saveImageMap').click(function() {
						var groupID = $('#hidden_device_selector').attr('group_id');
						$.post("update_imagemap.php", { 
							_id: groupID, 
							imagemap: myimgmap.getMapHTML() })
							.done(function(data) {								
								$('#imgmapeditor').siblings().show()
								$('#imgmapeditor').parents().siblings().show()
								$('#imgmapeditor').html('');
								$('#imgmapeditor').hide();
						});
					});
					$('#cancelmapping').click(function() {
						$('#imgmapeditor').siblings().show()
						$('#imgmapeditor').parents().siblings().show()
						$('#imgmapeditor').html('');
						$('#imgmapeditor').hide();
					});
				}//end request to imagemap editor
			);	
		});
		$('#current_groupid').val(id);

		$('#group .insert').hide();
		$('#group .update').show();
		$('#group .cancel').show();
		
		return false;
	});
	
	$('#group .cancel').click(function(){
		document.getElementById('group').reset();
		$('#groupdescription').html('');
		$('#current_groupid').val('');

		$('#cur_grouplogo').attr('src', '');
		$('#cur_groupmaps').attr('src', '');
		
		$('#edit_device_region').hide();
		$('#edit_group_region').hide();
		
		$('#groupid').removeAttr('readonly');
		
		return false;
	});
	$('.add_group').click(function() {
		document.getElementById('group').reset();
		$('#cur_grouplogo').removeAttr('src');
		$('#cur_groupmaps').removeAttr('src');
		$('#edit_device_region').hide();
		$('#edit_group_region').show();
		$('#group .insert').show();
		$('#group .update').hide();
		$('#group .cancel').show();
	});
	$('#group .insert').click(function(){
		_checkGroupForm(true);
	});
	$('#group .update').click(function(){
		_checkGroupForm(false);
	});
}
function _checkGroupForm(isInsert){
	var _groupid = $('#groupid');
	if (_groupid.val() == '') {
		alert('Please enter group ID');
		return false;
	}
	// check if new user name is duplicate or not
	var groupidArr = $('.reserve_gi');
	if (!isInsert) { // no need to check current username
		var _groupid_to_check = $('#current_groupid').val();
		for (var i=0; i<groupidArr.length; i++) {
			if ($(groupidArr[i]).val() == _groupid_to_check) {
				groupidArr.splice(i, 1);
				break;
			}
		}
	}
	for (var i=0; i<groupidArr.length; i++) {
		if (_groupid.val() == $(groupidArr[i]).val()) {
			alert('Given group ID is already taken. Please try again');
			return false;
		}
	}
	
	var _groupname = $('#groupname');
	if (_groupname.val() == '') {
		alert('Please enter group name');
		return false;
	}
	
	var op = 'update_group';
	if (isInsert) op = 'insert_group';
	
	$('#op').val(op);
	$('#currentGI').val($('#current_groupid').val());
	$('#group').submit();
}
$(document).ready(function(){
	initEditDevice();
	initEditGroup();
	$('#edit_group_region').hide();
	$('#edit_device_region').hide();
});