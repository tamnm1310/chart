<?php

$op = isset($_POST['removeCurrentDeviceIDSession']) ? $_POST['removeCurrentDeviceIDSession'] : '';
if ($op == 1) {
	session_start();
	unset($_SESSION['DeviceID']);
	echo 'unset via AJAX finished'; 
	die;
}


$currentDevice = '';
if (isset($_SESSION['DeviceID'])) $currentDevice = $_SESSION['DeviceID'];
$dao = new Dao();
$canReadGroup = '';
if ($_SESSION['user']->UserName != 'admin') {
	$canReadGroup = $dao->getUserPermission($_SESSION['user']->UserName, 1);
	$canReadGroup = '"'.join('","', $canReadGroup).'"';
}

$groups  = $dao->getAllDeviceGroups($canReadGroup);
$devices = $dao->getAllDevices($canReadGroup);

foreach ($devices as $d) {
	$groups[$d->DeviceGroupID]->child []= $d;
	if ($d->DeviceID == $currentDevice) {
		$groups[$d->DeviceGroupID]->className = 'active';
	}else {
		$groups[$d->DeviceGroupID]->className = 'deactive';
	}
}
// remove empty group
foreach ($groups as $k => $g) {
	if (empty($g->child)) unset($groups[$k]);
	else {
		echo '<div style="display:none" class="group_maps" id="g_'.$g->GroupID.'_map" imgSource="grp_image.php?f=m&g_id='.$g->GroupID.'">'.$g->InteractiveMaps.'</div>';
	}
}
?>

<div class="cc" style="background-color:#ECECEC; padding:2px 0;" id="logo_container">
	<?php foreach ($groups as $g) : ?>
	<img src="grp_image.php?f=l&g_id=<?= $g->GroupID; ?>" alt="<?= $g->GroupName; ?>" />
	<?php endforeach; ?>
</div>
<div style="position:relative">
    <div id="header" class="clearfix">
        <div id="top_welcome">
            <a href="../login.php?status=loggedout" title="Log out">Log out</a>
            <a href="profile.php" title="Edit your informaion">Edit profile</a>
            <?php if (!empty($_SESSION['user'])): ?>
                <span>Welcome, <?php echo $_SESSION['user']->FullName ? $_SESSION['user']->FullName : $_SESSION['user']->UserName; ?></span>		
            <?php endif; ?>
        </div>
    
        <ul id="menu">
            <li><a href="show.php" id="ico_chart" title="View device data as CHART"></a></li>
            <li><a href="table.php" id="ico_table" title="View device data as TABLE"></a></li>
            <li><a href="remove_data.php" title="Remove the old data" id="ico_remove_data" title="Remove device's data"></a></li>
            <?php if ($membership->can_User_Access_System_Settings()): ?>
            <li class="seperate"> </li>
            <?php endif; ?>
            
            <?php if ($membership->can_User_Access_System_Settings('userMgr')): ?>
            <li><a href="user.php" id="ico_user" title="User configuration"></a></li>
            <?php endif; ?>
            
            <?php if ($membership->can_User_Access_System_Settings('deviceMgr')): ?>
            <li><a href="device.php" id="ico_device" title="Device configuration"></a></li>
            <?php endif; ?>
            
            <?php if ($membership->can_User_Access_System_Settings('userMgr')): ?>
            <li><a href="permission.php" id="ico_permission" title="Permission configuration"></a></li>
            <?php endif; ?>
            
            <?php if ($membership->can_User_Access_System_Settings('deviceMgr')): ?>
            <li><a href="chart.php" id="ico_chart_config" title="Chart configuration"></a></li>
            <?php endif; ?>
        </ul>
    </div>
<!-- Close DIV in navi.php file-->