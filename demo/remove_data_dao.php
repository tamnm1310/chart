<?php

require_once '../classes/Membership.php';
require_once '../classes/conf.php';
require_once '../classes/dao.php';
$membership = new Membership();
$membership->confirm_Member();
$deviceId = $_POST['deviceID'];
$Ids = $_POST['Ids'];
$canModify = $membership->canModifyDeviceData($deviceId);
if ($canModify) {
	$dao = new Dao();
	$numberOfDeleted = $dao->deleteDataRecord($deviceId, $Ids);
	echo $numberOfDeleted . ' DataRecord deleted! Click OK to continue !';
} else {
	echo 'Access denied, you do not have permission enough to delete data of this device. Please contact your Administrator !';
}
?>