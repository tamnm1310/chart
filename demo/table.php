<?php

require_once '../classes/Membership.php';
require_once '../classes/conf.php';
require_once '../classes/dao.php';
$membership = new Membership();
$membership->confirm_Member();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link href="css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="css/default.css" />
    
	<script>action="table";</script>
    <?php if (!empty($_SESSION['DeviceID'])): ?>
    <script>DeviceID="<?= $_SESSION['DeviceID']; ?>";</script>
    <?php endif; ?>
	<?php include_once('_js_footer.php'); ?>
    <script src="js/bootstrap-paginator.js"></script>
    <title>Tabular data | Data logger</title>
</head>

<body>
<?php 
	include_once('navi.php');
	$rangeType = SELECT_BY_RANGE;
	$deviceId = empty($_SESSION['DeviceID'])?'':$_SESSION['DeviceID'];
	
	$range_include_time_option = 'range_dateonly';
	if (!empty($_SESSION['chart_range']) && !empty($_SESSION['chart_range'][$deviceId])) {
		$rangeType = empty($_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'])?SELECT_BY_RANGE:$_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'];
	}
	if (!empty($_SESSION['chart_range']) && !empty($_SESSION['chart_range'][$deviceId])) {
		$range_include_time_option = empty($_SESSION['chart_range'][$deviceId]['_rd_include_time'])?'range_dateonly':$_SESSION['chart_range'][$deviceId]['_rd_include_time'];
	}
?>
	<?php include_once('time_range_selector.php'); ?>
</body>
</html>