<?php
require_once '../classes/Membership.php';
require_once '../classes/conf.php';
require_once '../classes/dao.php';
$membership = new Membership();
$membership->confirm_Member();
$permission_mgr = $membership->can_User_Access_System_Settings('deviceMgr');
$isSuperUser = $membership->isSuperUser();
$userName = $membership->getUserName();

$accessDenied = true;
if ($permission_mgr || $isSuperUser) {
	$dao = new Dao();
	$data = $_POST['data'];	
	$dao->beginTransaction();
	$transuccess = 1;
	
	try {
		$manageableGroups = $dao->getAllManagableGroup($userName);
		$manageableGroupsId = array();
		foreach ($manageableGroups as $mg) {
			$manageableGroupsId[] = $mg->GroupID;
		}
		
		foreach ($data as $permission) {
			$sysPerm = $permission[0];
			$strSysPerm = '';
			$strUserName = $sysPerm[3];
			if ($isSuperUser && $strUserName!==$userName) {
			//Can not self upgrage system permission
			//Can not change system permission for other if he/she is not super user
				$strSysPerm ='';
				if ($sysPerm[0]==='1') {
						$strSysPerm .='userMgr,';
				}
				if ($sysPerm[1]==='1') {
					$strSysPerm .='deviceMgr,';
				}
				if ($sysPerm[2]==='1') {
					$strSysPerm .='super,';
				}
				if (strlen($strSysPerm)>0) {
					$strSysPerm = substr($strSysPerm, 0, strlen($strSysPerm)-1);
				}
				$accessDenied = false;
				$dao->updateUserSysPermission($strUserName, $strSysPerm);
			}

			$groupPermissions = $permission[1];
			
			foreach ($groupPermissions as $groupPerm) {
			
				if (in_array($groupPerm[0], $manageableGroupsId) ){
					$dao->updateOrInsertUserGroupPermission($strUserName, $groupPerm[0], $groupPerm[1], $groupPerm[2], $groupPerm[3], $groupPerm[4]);
					$accessDenied = false;
				}
			}
		}
	} catch (Exception $e) {
		echo 'Error while running update permission';
		$transuccess = 0;
		$dao->rollbackTransaction();
	}
	if ($accessDenied === true) {
		$dao->rollbackTransaction();
		echo 'Access denied !';
	}
	if ($transuccess===1 ) {
		$dao->commitTransaction();
		echo 'Permission saved !';
	}
} else {
	echo 'Access denied !';
}
?>