<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-datetimepicker.min.css">
<!-- Open DIV in time_range_selector -->
<div id="chart_wrapper">
	<div id="container" class="chart_container">
        <span class="place_holder">
            <?php if (empty($_SESSION['DeviceID'])): ?>
            Please select device to view data
            <?php else: ?>
            <img src="images/loading.gif">
            <?php endif; ?>
        </span>
    </div>
	<div id="redrawContainer" style="<?php if (empty($deviceId)): ?>display:none<?php endif; ?>"  class="form-inline">
		<select id="rangeTypeSelector" name="rangeTypeSelector">
			<option value="selectedByRange">From</option>
			<option value="selectLastPeriod">Last</option>
		</select>
		<span id="drawByRange" style="<?php if ($rangeType!=SELECT_BY_RANGE): ?>display:none<?php endif; ?>">			
				<div id="datetimepickerstart" class="input-append date">
				  <input type="text" id="rd_startdate"></input>
				  <span class="add-on">
					<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
				  </span>
				</div>
				<div id="datetimepickerend" class="input-append date">
				  <input type="text" id="rd_enddate"></input>
				  <span class="add-on">
					<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
				  </span>
				</div>
			<div class="btn-group" data-toggle="buttons-radio" id="range_include_time_option">
				<button type="button" class="btn btn-small btn-info <?php if ($range_include_time_option=='range_dateonly'): ?>active<?php endif; ?> range_dateonly" value="range_dateonly">Date only</button>
				<button type="button" class="btn btn-small btn-info <?php if ($range_include_time_option!='range_dateonly'): ?>active<?php endif; ?> range_datetime" value="range_datetime">Date time</button>				
			</div>
			<span>&nbsp;</span>
		</span>
		<span id="drawPeriod" style="<?php if ($rangeType!=SELECT_BY_PERIOD): ?>display:none<?php endif; ?>">
			<input type="text" id="rangeCount" name="rangeCount"/>
			<select id="rangeNotation" name="rangeTypeSelector">
				<option value="Min">Minutes</option>
				<option value="Hour">Hours</option>
				<option value="Day">Days</option>
				<option value="Week">Weeks</option>
			</select>
			<span>&nbsp;</span>
		</span>
		<input type="button" value="Apply" id="redrawChart" data-id='' class="btn btn-primary" />
	</div>
</div>
<!-- Close div in time_range_selector -->
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
	var time_format ="<?php
		if ($range_include_time_option =='range_dateonly') {
			echo 'yyyy-MM-dd';
		}else {
			echo 'yyyy-MM-dd hh:mm:ss';
		}
	?>";
	var storedHTML = $('#drawByRange').html();
    $(document).ready(function(){
		$('#datetimepickerstart').datetimepicker({
			format: time_format,
			language: 'en'
		});
		$('#datetimepickerend').datetimepicker({
			format: time_format,
			language: 'en'
		});		
		$("#rangeTypeSelector").click(function(){
			if ($("#rangeTypeSelector").get(0).value=='selectLastPeriod') {
				$("#drawByRange").css("display", "none");
				$("#drawPeriod").css("display", "inline");
			}else if ($("#rangeTypeSelector").get(0).value=='selectedByRange'){
				$("#drawPeriod").css("display", "none");
				$("#drawByRange").css("display", "inline");
			}
		});
	});
</script>