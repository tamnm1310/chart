<?php
require_once '../classes/Membership.php';
require_once '../classes/conf.php';
require_once '../classes/dao.php';
require_once '../classes/util.php';
$membership = new Membership();
$membership->confirm_Member();
$access_sys = $membership->can_User_Access_System_Settings();
if ($access_sys) {
	$op = @$_GET['op'];
	if (empty($op)) $op = __getParam('op');
	
	$dao = new Dao();
	$_data = array();
	$data = array();
	
	switch ($op) {
		case 'update_group':
		case 'insert_group':
			$redirect = true;
			$redirect_url = $_SERVER['HTTP_REFERER'];
			
			$data = $_POST;
		break;
		
		default:
			$_data = @explode('&', $_GET['data']);
			
			foreach ($_data as $str) {
				$str = explode('=', $str);
				switch ($str[0]) {
					case 'deviceid': 
					case 'groupid':
						$str[1] = str_replace(' ', '', $str[1]);
						$data[$str[0]] = convert($str[1]);
					break;
					default:
						$data[$str[0]] = convert($str[1]);
					break; 
				}
			}
		break;
	}
	
	$data['currentDI'] = $_GET['currentDI'];
	
	if (!empty($_FILES)) { // upload file first
		if ($op == 'update_group' || $op == 'insert_group') {
			$allowedExts = array("gif", "jpeg", "jpg", "png");
			// group logo
			$extension = end(explode(".", $_FILES["grouplogo"]["name"]));
			if (in_array($extension, $allowedExts)) {
				if (isset($_FILES['grouplogo']) && $_FILES['grouplogo']['size'] > 0) { 
					// Temporary file name stored on the server
					$tmpName  = $_FILES['grouplogo']['tmp_name'];
					// Read the file 
					$fp       = fopen($tmpName, 'r');
					$grp_logo_data = fread($fp, filesize($tmpName));
					$grp_logo_data = addslashes($grp_logo_data);
					fclose($fp);
					$data['grouplogo'] = $grp_logo_data;
				}
			}
			
			// group map
			$extension = end(explode(".", $_FILES["groupmaps"]["name"]));
			if (in_array($extension, $allowedExts)) {
				if (isset($_FILES['groupmaps']) && $_FILES['groupmaps']['size'] > 0) { 
					// Temporary file name stored on the server
					$tmpName  = $_FILES['groupmaps']['tmp_name'];
					// Read the file 
					$fp       = fopen($tmpName, 'r');
					$grp_map_data = fread($fp, filesize($tmpName));
					$grp_map_data = addslashes($grp_map_data);
					fclose($fp);
					$data['groupmaps'] = $grp_map_data;
				}
			}
		}
	}
	
	switch ($op) {
		case 'delete_device':
			$deviceid = convert($_GET['deviceid']);
			$dao->removeDevice($deviceid);
		break;
		
		case 'insert_device':
			$dao->addNewDevice($data);
		break;
		
		case 'update_device':
			$dao->updateDevice($data);
		break;
		
		case 'delete_group':
			$groupid = convert($_GET['groupid']);
			$dao->removeGroup($groupid);
		break;
		
		case 'insert_group':
			$dao->addNewGroup($data);
		break;
		
		case 'update_group':
			$dao->updateGroup($data);
		break;
	}
	if (isset($redirect) && $redirect == true) {
		header('location: '.$redirect_url);
		exit;
	}
	echo 1;
} else {
	echo 0;
}
?>