<script language="javascript" type="text/javascript" src="js/imgmap/imgmap.js"></script>
<script language="javascript" type="text/javascript" src="js/imgmap/default_interface.js"></script>
<link rel="stylesheet" href="css/imgmap.css" type="text/css">
<?php
	require_once '../classes/util.php';
	require_once '../classes/dao.php';
	require_once '../classes/conf.php';
	$groupID = __getParam('g_id');
	$dao = new Dao();
	$d_list = array();
	if (!empty($groupID)) {
		$groupID ='"'.$groupID.'"';
		$devices = $dao->getAllDevices($groupID);
		foreach ($devices as $d) {
			$d_list []= $d;
		}
	}
	//var_dump($d_list);
	$image_link = __getParam('imgLink');
?>

<div id="image_editor_container" >
	<div style="display:none">
		<select id='hidden_device_selector' group_id=<?php echo $groupID;?>>
			<option selected>Select device</option>
			<?php
			foreach ($devices as $d) {
				?><option value="javascript:showDevice('<?php echo $d->DeviceID; ?>')"> <?php echo $d->DeviceID.' : '.$d->DeviceName;?></option>
			<?php }	?>
		</select>
	</div>
	
	<fieldset>
		<legend style="width:100%">
			<a onclick="toggleFieldset(this.parentNode.parentNode)">Image map areas</a>
			<a class="btn" id='cancelmapping' style="float: right; margin: 0 3px">Close</a>
			<a class="btn btn-primary" id='saveImageMap' style="float: right; margin: 0 3px">Update</a>
			
		</legend>
		<div style="border-bottom: solid 1px #efefef">
		<div id="button_container">
			<!-- buttons come here -->
			<img src="images/add.gif" onclick="myimgmap.addNewArea()" alt="Add new area" title="Add new area"/>
			<img src="images/delete.gif" onclick="myimgmap.removeArea(myimgmap.currentid)" alt="Delete selected area" title="Delete selected area"/>
			<img src="images/zoom.gif" id="i_preview" onclick="myimgmap.togglePreview();" alt="Preview image map" title="Preview image map"/>
			<img src="images/html.gif" onclick="gui_htmlShow()" alt="Get image map HTML" title="Get image map HTML"/>
			<label for="dd_zoom">Zoom:</label>
			<select onchange="gui_zoom(this)" id="dd_zoom" class="dd_zoom">
			<option value='0.25'>25%</option>
			<option value='0.5'>50%</option>
			<option value='1' selected="1">100%</option>
			<option value='2'>200%</option>
			<option value='3'>300%</option>
			</select>
			<input type="hidden" id="dd_output" value="imagemap"/>
		</div>
		<div id="select_visible_label" style="float:right">
			<select onchange="changelabeling(this)">
			<option value=''>No labeling</option>
			<option value='%n'>Label with numbers</option>
			<option value='%a'>Label with alt text</option>
			<option value='%h'>Label with href</option>
			<option value='%c' selected='1'>Label with coords</option>
			</select>
		</div>
		</div>
		<div id="form_container" style="clear: both;">
		<!-- form elements come here -->
		</div>
	</fieldset>
	<fieldset>
		<legend>
			<a onclick="toggleFieldset(this.parentNode.parentNode)">Image</a>
		</legend>
		<div id="pic_container"></div>			
	</fieldset>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		gui_loadImage('<?php echo $image_link ?>');
	});
</script>