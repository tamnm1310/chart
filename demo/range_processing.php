<?php
session_start();
require_once '../classes/util.php';
require_once '../classes/conf.php';
include_once('../classes/dao.php');

$deviceId = __getParam('id');
$isDefault = false;
$rangeTypeSelector = __getParam('rtl');
$endDate='';
$startDate='';
$rangeCount='';
$rangeNotation='';
$includeTime ='';
$isSession = false;
$isDefault = true;
//var_dump($_SESSION['chart_range']);

if (empty($deviceId)) { 
	$deviceId = $_SESSION['DeviceID'];
} else {
	$_SESSION['DeviceID'] = $deviceId;
}
if (!empty($deviceId)) {
	if (empty($rangeTypeSelector)) {	
		$isSession=true;
	}
	if ($isSession) {
		if (!empty($_SESSION['chart_range'])) {
			if (!empty($_SESSION['chart_range'][$deviceId])) {
				$rangeTypeSelector = $_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'];
				if (!empty($rangeTypeSelector)) {
					if ($rangeTypeSelector===SELECT_BY_RANGE) {
						$endDate = $_SESSION['chart_range'][$deviceId]['_rd_enddate'];
						$startDate = $_SESSION['chart_range'][$deviceId]['_rd_startdate'];
						$includeTime = $_SESSION['chart_range'][$deviceId]['_rd_include_time'];
						if (!empty($endDate)&&!empty($startDate)&&!empty($includeTime)) {
							if ($includeTime==='range_dateonly'||$includeTime==='range_datetime') {
								$isDefault = (strtotime($startDate)===false) || (strtotime($endDate)===false);
							}
						}
					}else if ($rangeTypeSelector===SELECT_BY_PERIOD){						
						$rangeCount = $_SESSION['chart_range'][$deviceId]['_rd_rangecount'];
						$rangeNotation = $_SESSION['chart_range'][$deviceId]['_rd_rangetypenotation'];
						if (!empty($rangeCount)&&!empty($rangeNotation)) {
							$endDate = strtotime(date('Y-m-d H:i:s'));	
							$startDate = strtotime(strtotime($endDate)." - " . $rangeCount . " " . $rangeNotation);
							$isDefault = false;
							//error_log($rangeCount . ' : ' . $isDefault . ' : ' . (strtotime($startDate)===false)) ;
							if (!$isDefault) {
								$startDate = date('Y-m-d H:i:s', $startDate);
								$endDate = date('Y-m-d H:i:s', $endDate);
							}
						}
					}
				}
			}//Data was included in session variable
		}
	}else {
		//param included in url
		if ($rangeTypeSelector===SELECT_BY_RANGE) {
			$startDate = __getParam('s');
			$endDate = __getParam('e');
			$includeTime = __getParam('ht');
			if (!empty($endDate)&&!empty($startDate)&&!empty($includeTime)) {
				if ($includeTime==='range_dateonly'||$includeTime==='range_datetime') {
					$startDate.=$includeTime==='range_dateonly'?' 00:00:00':'';
					$endDate.=$includeTime==='range_dateonly'?' 23:59:59':'';
					$isDefault = (strtotime($startDate)===false) || (strtotime($endDate)===false);
					if (!$isDefault) {
						$_SESSION['chart_range'][$deviceId]['_rd_startdate'] = $startDate;
						$_SESSION['chart_range'][$deviceId]['_rd_enddate'] = $endDate;
						$_SESSION['chart_range'][$deviceId]['_rd_include_time'] = $includeTime;
						$_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'] = $rangeTypeSelector;
					}
				}
			}
		}else if ($rangeTypeSelector===SELECT_BY_PERIOD) {
			$rangeCount = __getParam('rc');
			$rangeNotation = __getParam('rn');
			if (!empty($rangeCount)&&!empty($rangeNotation)) {
				$endDate = strtotime(date('Y-m-d H:i:s'));	
				$startDate = strtotime(strtotime($endDate)." - " . $rangeCount . " " . $rangeNotation);
				if ($startDate!==false&&$endDate!==false) {
					$ret['_rd_rangecount'] = $rangeCount;
					$ret['_rd_rangetypeselector'] = $rangeTypeSelector;
					$ret['_rd_rangetypenotation'] = $rangeNotation;
					$_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'] = SELECT_BY_PERIOD;
					$_SESSION['chart_range'][$deviceId]['_rd_rangecount'] = $rangeCount;
					$_SESSION['chart_range'][$deviceId]['_rd_rangetypenotation'] = $rangeNotation;
					$startDate = date('Y-m-d H:i:s', $startDate);
					$endDate = date('Y-m-d H:i:s', $endDate);
					$isDefault=false;
				}
			}
		}
	}
	$_SESSION['DeviceID'] = $deviceId;
	include_once('../classes/dao.php');
    include_once('../classes/conf.php');

    $dao = new Dao();
    $ret = array();
	// get device information
	$device = $dao->getDeviceByID($deviceId);
	$defaultRange = $device->ChartRange;
	$defaultRangeNotation = $device->ChartRangeNotation;
	$recordInterval = $device->RecordInterval;
	$maxChartSample = $device->MaxChartSample;
	if (empty($maxChartSample)) $maxChartSample = MAX_CHART_SAMPLE;
	if ($isDefault) {
		//Still empty, load the range from database
		$endDate = strtotime(date('Y-m-d H:i:s'));
		$startDate = strtotime(strtotime($endDate)." - " . $defaultRange . " " . $defaultRangeNotation);
		//error_log("From ".date('Y-m-d H:i:s ', $startDate)." to ".date('Y-m-d H:i:s ', $endDate));
		$rangeTypeSelector = SELECT_BY_PERIOD;
		$_SESSION['chart_range'][$deviceId]['_rd_rangetypeselector'] = SELECT_BY_PERIOD;
		$_SESSION['chart_range'][$deviceId]['_rd_rangecount'] = $defaultRange;
		$_SESSION['chart_range'][$deviceId]['_rd_rangetypenotation'] = $defaultRangeNotation;
		$_SESSION['chart_range'][$deviceId]['_rd_include_time'] = 'range_dateonly';
		$startDate = date('Y-m-d H:i:s', $startDate);
		$endDate = date('Y-m-d H:i:s', $endDate);
	}
}
?>